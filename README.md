# Lab 06 - Value Returning Methods

We will be following the exercises in [Thinking in Java Chapter 6](http://greenteapress.com/thinkapjava/html/thinkjava008.html#toc52)

## Getting Started
1. Open a terminal, command prompt, or PowerShell and change to your working directory (e.g. U:)
2. Grab a copy of lab06 using: `git clone https://hohonuuli@bitbucket.org/csis10a/lab06.git`
3. Follow the instructions at [https://docs.google.com/document/d/19Kgr5AcWl48I55vwm6J_nUZ_wefBOkXNjT42Ks9uFyE/edit?usp=sharing](https://docs.google.com/document/d/19Kgr5AcWl48I55vwm6J_nUZ_wefBOkXNjT42Ks9uFyE/edit?usp=sharing)

__Note:__ If Git is not installed on your computer. You can download the lab from [https://bitbucket.org/csis10a/lab06/get/master.zip](https://bitbucket.org/csis10a/lab06/get/master.zip)